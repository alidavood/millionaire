from django.core.exceptions import ValidationError
from django import forms
from django.utils.translation import ugettext as _

from .models import QuestionOption


class QuestionOptionAdminForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data['is_correct'] and cleaned_data['question'].correct_option:
            raise ValidationError(_('Each question can only have one correct option'))

        return cleaned_data

    class Meta:
        model = QuestionOption
        fields = '__all__'


class QuestionOptionForm(forms.Form):
    answer = forms.ModelChoiceField(queryset=QuestionOption.objects.all())

    def clean_answer(self):
        if not self.cleaned_data['answer'].is_correct:
            raise ValidationError(_("Invalid answer"))
        return self.cleaned_data['answer']
